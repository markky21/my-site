import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {routerModule} from './app.routing';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {SlickModule} from 'ngx-slick';

import {AppComponent} from './app.component';
import {MainNavModule} from './main-nav/main-nav.module';
import {BackgroundModule} from './background/background.module';
import {HomeModule} from './home/home.module';
import {AboutModule} from './about/about.module';
import {SkillsModule} from './skills/skills.module';
import {ContactModule} from './contact/contact.module';
import {FooterComponent} from './footer/footer.component';
import {LogoModule} from './logo/logo.module';
import {PageModule} from './page/page.module';
import {HttpModule} from '@angular/http';
import {MessageModule} from './message/message.module';
import {AppService} from './app.service';
import {BlogModule} from './blog/blog.module';
import {Ng2DeviceDetectorModule} from 'ng2-device-detector';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    MainNavModule,
    BackgroundModule,
    routerModule,
    HomeModule,
    AboutModule,
    SkillsModule,
    ContactModule,
    BrowserAnimationsModule,
    SlickModule.forRoot(),
    LogoModule,
    PageModule,
    HttpModule,
    MessageModule,
    BlogModule,
    Ng2DeviceDetectorModule.forRoot(),
  ],
  providers: [
    AppService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
