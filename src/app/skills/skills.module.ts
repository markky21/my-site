import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SkillsComponent} from './skills.component';
import {SlickModule} from 'ngx-slick';

@NgModule({
  imports: [
    CommonModule,
    SlickModule
  ],
  declarations: [SkillsComponent],
  exports: [
    SkillsComponent
  ]
})
export class SkillsModule {
}
