import {Component, OnInit, ViewChild, OnDestroy} from '@angular/core';
import {Http} from '@angular/http';
import {AppService} from '../app.service';
import {slideUpFadeIn} from '../_animations/slideUpFadeIn';

import * as $ from 'jquery';

import {Skill} from '../_interfaces/interfaces';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss'],
  animations: [slideUpFadeIn]
})
export class SkillsComponent implements OnInit, OnDestroy {

  @ViewChild('slickModal') slickModal;

  navSlideState = 'hidden';
  $slick: JQuery;
  skills: Skill[];
  animateText = false;

  slideConfig = {
    'slidesToShow': 6,
    'slidesToScroll': 1,
    'variableWidth': true,
    'infinite': true,
    'dots': false,
    'arrows': false,
    'autoplay': true,
    'autoplaySpeed': 500,
    'speed': 2000,
  };

  constructor(private appService: AppService, private http: Http) {
  }

  ngOnInit() {

    this.http.get('/assets/data/skills.json')
      .subscribe(res => {
        this.skills = res.json().skills;
      });

    setTimeout(() => {
      this.navSlideState = 'visible';
    }, 500);

    this.$slick = $(this.slickModal.el.nativeElement);
    setTimeout(() => {
      this.$slick.css('opacity', 1);
    }, 3000);

    setTimeout(() => {
      this.animateText = true;
    }, 2000);
  }

  ngOnDestroy() {
    this.$slick.css('opacity', 0);
  }

  afterChange(event): void {
    // console.log(event);
    $('.slick-current h2').click();
  }

  beforeChange(event): void {
    // console.log(event);
  }
}
