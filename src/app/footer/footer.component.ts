import {Component, OnInit, OnDestroy} from '@angular/core';
import {AppService} from '../app.service';
import {Subscription} from 'rxjs/Subscription';

import {CurrentPage} from "../_interfaces/interfaces";

@Component({
  selector: 'app-footer',
  template: `
    <footer [ngClass]="currentPage ? currentPage.router + '-page' : ''">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="social-media-wrapper">
              <a href="https://www.facebook.com/marcin.mirecki.7" target="_blank">
                <i class="fa fa-facebook-square" aria-hidden="true"></i>
              </a>
              <a class="underlined" href="https://www.facebook.com/marcin.mirecki.7" target="_blank">
                <span class="text">https://www.facebook.com/marcin.mirecki.7</span>
              </a>
            </div>
            <div class="social-media-wrapper">
              <a href="https://www.linkedin.com/in/marcin-mirecki-8747718b/" target="_blank">
                <i class="fa fa-linkedin-square" aria-hidden="true"></i>
              </a>
              <a class="underlined" href="https://www.linkedin.com/in/marcin-mirecki-8747718b/" target="_blank">
                <span class="text">https://www.linkedin.com/in/marcin-mirecki-8747718b/</span>
              </a>
            </div>
            <div class="social-media-wrapper">
              <a href="tel:791429911" target="_blank">
                <i class="fa fa-phone-square" aria-hidden="true"></i>
              </a>
              <a class="underlined" href="tel:791429911" target="_blank">
                <span class="text">(+48) 791 42 99 11</span>
              </a>
            </div>
            <div class="social-media-wrapper">
              <a href="mailto:markky21@gmail.com" target="_blank">
                <i class="fa fa-envelope-square" aria-hidden="true"></i>
              </a>
              <a class="underlined" href="mailto:markky21@gmail.com" target="_blank">
                <span class="text">markky21@gmail.com</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  `,
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit, OnDestroy {

  currentPageSubscription: Subscription;
  currentPage: CurrentPage;

  constructor(private appService: AppService) {
  }

  ngOnDestroy() {
    this.currentPageSubscription.unsubscribe();
  }

  ngOnInit() {

    this.currentPageSubscription = this.appService.getCurrentPage()
      .subscribe((currentPage) => {
        this.currentPage = currentPage;
      });

  }

}
