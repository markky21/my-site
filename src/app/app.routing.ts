import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {AboutComponent} from './about/about.component';
import {SkillsComponent} from './skills/skills.component';
import {ContactComponent} from './contact/contact.component';
import {HomeTextComponent} from './home/home-text.component';
import {HomeNavComponent} from './home/home-nav.component';
import {PageComponent} from './page/page.component';
import {BlogComponent} from './blog/blog.component';

const routerConfig: Routes = [
  {path: '', redirectTo: 'home/welcome', pathMatch: 'full'},
  {path: 'page', redirectTo: 'home/overview', pathMatch: 'full'},
  {
    path: 'page', component: PageComponent, children: [
    {path: 'about', component: AboutComponent, data: {state: 'about', animation: 'about'}},
    {path: 'skills', component: SkillsComponent, data: {state: 'skills', animation: 'skills'}},
    {path: 'contact', component: ContactComponent, data: {state: 'contact', animation: 'contact'}},
    {path: 'blog', component: BlogComponent, data: {state: 'blog', animation: 'blog'}},
  ], data: {state: 'page', animation: 'page'}
  },
  {path: 'home', redirectTo: 'home/welcome', pathMatch: 'full'},
  {
    path: 'home', component: HomeComponent, children: [
    {path: 'welcome', component: HomeTextComponent, data: {state: 'homeWelcome', animation: 'home-text'}},
    {path: 'overview', component: HomeNavComponent, data: {state: 'homeOverview', animation: 'home-nav'}},
  ], data: {state: 'home', animation: 'home'}
  },
  {path: '**', redirectTo: '/home/welcome'},
];
export const routerModule = RouterModule.forRoot(routerConfig, {
  enableTracing: false,
  useHash: false,
});

