import {Component, OnInit} from '@angular/core';
import {slideUpFadeIn} from '../_animations/slideUpFadeIn';


@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  animations: [slideUpFadeIn]
})
export class AboutComponent implements OnInit {

  backgroundImg: string;
  animateText = false;

  constructor() {
  }

  ngOnInit() {
    this.backgroundImg = `../../assets/imgs/wroclaw/${Math.floor(Math.random() * 4 + 1)}.jpg`;

    setTimeout(() => {
      this.animateText = true;
    }, 2000);
  }

}
