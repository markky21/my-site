import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainNavComponent} from './main-nav.component';
import {routerModule} from "../app.routing";

import {WaveBockgroundComponent} from '../svg/wave-bockground.component';
import {LogoComponent} from '../svg/logo.component';

@NgModule({
  imports: [
    CommonModule,
    routerModule
  ],
  declarations: [
    MainNavComponent,
    WaveBockgroundComponent,
    LogoComponent,
  ],
  exports: [
    MainNavComponent,
    LogoComponent
  ]
})
export class MainNavModule {
}
