import {Component, OnInit, ViewChild} from '@angular/core';
import {AppService} from '../app.service';
import {Subscription} from 'rxjs/Subscription';

import {CurrentPage} from '../_interfaces/interfaces';

@Component({
  selector: 'app-main-nav',
  template: `
    <header
      [ngClass]="currentPage ? currentPage.router + '-page' : ''"
    >

      <div class="header-bottom" *ngIf="deviceInfo.device !== 'android'">
        <app-wave-bockground></app-wave-bockground>
        <app-wave-bockground></app-wave-bockground>
        <app-wave-bockground></app-wave-bockground>
      </div>

      <div class="container" [ngClass]="deviceInfo ? deviceInfo.device + '-device' : ''">
        <div class="row">
          <div class="col-xs-12">
            <div class="nav-wrapper">
              <nav (mouseenter)="onLogoHover($event)" (mouseleave)="onLogoHover($event)">

                <div routerLink="/blank" class="main-logo">
                  <app-logo></app-logo>
                  <div class="full-name">
                    <div #name class="name fade-in-animation" [ngClass]="{'animate':  this.showNav}">arcin</div>
                    <div #lastname class="lastname fade-in-animation" [ngClass]="{'animate':  this.showNav}">irecki
                    </div>
                  </div>
                </div>

                <ul [ngClass]="{'active': this.showNav}">
                  <li *ngFor="let site of sites"
                      routerLink="/page/{{site.router}}"
                      [ngClass]="{'active': site.router === currentPage?.router }"
                  >{{site.name}}
                  </li>
                </ul>

              </nav>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </header>
  `,
  styleUrls: ['main-nav.component.scss']
})
export class MainNavComponent implements OnInit {

  deviceInfo;
  sites: CurrentPage[];
  showNav: boolean;
  currentPage: CurrentPage;
  currentPageSubscription: Subscription;

  @ViewChild('name') name;
  @ViewChild('lastname') lastname;


  constructor(private appService: AppService) {
  }

  ngOnInit() {
    this.appService.getSites().subscribe(res => {
      this.sites = res.json().sites;
    });
    this.appService.animateText([this.name.nativeElement, this.lastname.nativeElement]);

    this.currentPageSubscription = this.appService.getCurrentPage()
      .subscribe((currentPage) => {
        this.currentPage = currentPage;
      });

    setTimeout(() => {
      if (window.innerWidth < 576) {
        this.showNav = true;
      }
    }, 4000);

    this.deviceInfo = this.appService.getDeviceInfo();
  }

  onLogoHover($event): void {
    if (window.innerWidth > 576) {
      this.showNav = !this.showNav;
    } else {
      this.showNav = true;
    }
  }

}
