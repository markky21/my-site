import {Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import {AppService} from '../app.service';
import {Subscription} from 'rxjs/Subscription';
import {pageChangeAnimation} from '../_animations/pageChageAnimation';

import * as $ from 'jquery';

import {CurrentPage} from '../_interfaces/interfaces';

@Component({
  selector: 'app-page',
  template: `
    <div #wrapper class="wrapper" [ngClass]="currentPage?.router + '-page'">

      <nav *ngIf="showNav" #nav class="page-nav">
        <div
          *ngFor="let site of sites"
          attr.data-page="{{site.router}}"
          [ngClass]="{'active': currentPage.router.indexOf(site.router) > -1}"
          class="nav-element">
          <h2 routerLink="/page/{{site.router}}">
            <span>{{site.name}}</span>
          </h2>
        </div>
      </nav>

      <article
        #pageArticle
        [@pageChangeAnimation]="getRouteAnimation(route)"
        (@pageChangeAnimation.start)="animationStarted($event)"
        class="page-content">
        <router-outlet #route="outlet"></router-outlet>
      </article>

    </div>
  `,
  styleUrls: ['./page.component.scss'],
  animations: [
    pageChangeAnimation,
  ]
})
export class PageComponent implements OnInit, OnDestroy {

  @ViewChild('nav') nav;
  @ViewChild('wrapper') wrapper;
  @ViewChild('pageArticle') pageArticle;

  currentPage: CurrentPage;
  sites: CurrentPage[];
  showNav = false;
  currentPageSubscription: Subscription;

  constructor(private appService: AppService) {
  }

  ngOnDestroy() {
    this.currentPageSubscription.unsubscribe();
  }

  ngOnInit() {
    this.appService.getSites().subscribe(res => {
      this.sites = res.json().sites;
    });

    this.currentPageSubscription = this.appService.getCurrentPage()
      .subscribe((currentPage) => {
        if (!this.currentPage || this.currentPage.router !== currentPage.router) {
          this.currentPage = currentPage;
          this.showNav = true;
          this.setPosition();
        }
      });
  }

  animationStarted($event): void {
    const target = $('app-' + $event.toState);

    $(this.pageArticle.nativeElement)
      .css('transition', 'opacity ' + $event.totalTime + 'ms ease')
      .css('opacity', 0);

    setTimeout(() => {
      target.css('display', 'block');
      setTimeout(() => {
        $(this.pageArticle.nativeElement).css('opacity', 1);
      }, 400);
    }, $event.totalTime + 100);
  }

  getRouteAnimation(outlet) {
    if (outlet.activatedRouteData) {
      return outlet.activatedRouteData.animation;
    }
  }

  setPosition(): void {
    $('html,body').animate({scrollTop: 0}, 'slow', 'swing', () => {
      setTimeout(() => {
        if (this.wrapper && this.nav) {
          const $wrapper = $(this.wrapper.nativeElement);
          const $nav = $(this.nav.nativeElement);
          const currentTranslation = parseInt($nav.css('margin-left'));
          const translation = -($nav.find('.active').offset().left - $wrapper.offset().left - currentTranslation) + 15;
          $nav.css('margin-left', translation + 'px');
        }
      }, 500);
    });
  }
}
