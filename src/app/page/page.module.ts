import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PageComponent} from './page.component';

import {routerModule} from '../app.routing';
import {SlickModule} from 'ngx-slick';

@NgModule({
  imports: [
    CommonModule,
    routerModule,
    SlickModule,
  ],
  declarations: [PageComponent]
})
export class PageModule {
}
