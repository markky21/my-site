import {Component, OnInit, ViewChild} from '@angular/core';
import {initAnimation} from './_animations/initSlide';
import {pageChangeAnimation} from './_animations/pageChageAnimation';
import {AppService} from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  animations: [
    initAnimation,
    pageChangeAnimation
  ]
})
export class AppComponent implements OnInit {

  @ViewChild('routerWrapper') routerWrapper;
  showHideMainAnimation = 'show';
  deviceInfo: object;

  constructor(private appService: AppService) {


  }

  ngOnInit() {
    setTimeout(() => {
      this.showHideMainAnimation = 'hide';
    }, 3000);

    this.deviceInfo = this.appService.getDeviceInfo();

    this.appService.getCurrentPage().subscribe((newPage) => {
      this.hideShowMainAnimation();
    });
  }

  animationStarted($event) {
    const target = $('app-' + $event.toState);

    $(this.routerWrapper.nativeElement)
      .css('transition', 'opacity ' + $event.totalTime + 'ms ease')
      .css('opacity', 0);

    setTimeout(() => {
      target.css('display', 'block');
      setTimeout(() => {
        $(this.routerWrapper.nativeElement).css('opacity', 1);
      }, 400);
    }, $event.totalTime + 100);
  }

  getRouteAnimation(outlet) {
    if (outlet.activatedRouteData) {
      return outlet.activatedRouteData.animation;
    }
  }

  hideShowMainAnimation() {
    // TODO zmienić tak aby na animationEnd zaczynało odliczanie
    if (this.showHideMainAnimation === 'hide') {
      this.showHideMainAnimation = 'show';
      setTimeout(() => {
        this.showHideMainAnimation = 'hide';
      }, 1000);
    }
  }
}
