import {Component, OnInit} from '@angular/core';
import {MessageService} from './message.service';

@Component({
  selector: 'app-message',
  template: `
    <div class="message-container" [ngClass]="{'error': isError, 'show-message': showMessage}">
      <div class="message-wrapper">
        <div class="message-text">
          {{message}}
        </div>
        <span class="close-btn" (click)="hideMessage($event)">
        <i class="fa fa-times" aria-hidden="true"></i>
      </span>
      </div>
    </div>
  `,
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {

  message: string;
  isError: boolean;
  showMessage: boolean;

  constructor(private messageService: MessageService) {
  }

  ngOnInit() {
    this.messageService.getCurrentMessage().subscribe((message) => {
      this.message = message.message;
      this.isError = message.isError;
      this.showMessage = true;
    });
  }

  hideMessage($event): void {
    this.showMessage = false;
  }

}
