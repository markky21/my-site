import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class MessageService {

  private messageSubject = new Subject<object>();

  constructor() {
  }

  setCurrentMessage(currentPage: object): void {
    this.messageSubject.next(currentPage);
  }

  getCurrentMessage(): Observable<any> {
    return this.messageSubject.asObservable();
  }

}
