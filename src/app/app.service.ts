import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Http} from '@angular/http';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Router, NavigationEnd} from '@angular/router';
import {Ng2DeviceService} from 'ng2-device-detector';

import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import {CurrentPage, IMessage} from './_interfaces/interfaces';

declare let ga: Function;

@Injectable()
export class AppService {

  private emailUrl = 'http://marcin-mirecki.pl/assets/email.php';

  sites;
  routerEvent;
  currentPage: CurrentPage = {
    name: 'home',
    title: '',
    shortDescription: '',
    router: 'home',
    button: '',
  };

  private currentPageSubject = new BehaviorSubject<object>(this.currentPage);

  constructor(private router: Router, private http: Http, private deviceService: Ng2DeviceService) {

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.routerEvent = event;

        ga('set', 'page', this.routerEvent.urlAfterRedirects);
        ga('send', 'pageview');

        if (this.sites) {
          this.setCurrentPage(this.sites);
        }
      }
    });

    this.getSites().subscribe((sites) => {
      this.sites = sites.json().sites;

      if (this.routerEvent) {
        this.setCurrentPage(sites.json().sites);
      }
    });
  }

  setCurrentPage(sites): void {

    let newPage: CurrentPage;
    for (const site of sites) {
      if (this.routerEvent.url.indexOf(site.router) >= 0) {
        newPage = site;
      }
    }
    if (!newPage) {
      newPage = {
        name: 'home',
        title: '',
        shortDescription: '',
        router: this.routerEvent.url,
        button: '',
      };
    }

    if (!this.currentPage || this.currentPage.router !== newPage.router) {
      this.currentPage = newPage;
      this.currentPageSubject.next(newPage);
    }
  }

  getCurrentPage(): Observable<any> {
    return this.currentPageSubject.asObservable();
  }


  getSites(): Observable<any> {
    return this.http.get('/assets/data/sites.json');
  }

  animateText(nodes: Array<any>, tag?: string, delay?: number) {
    for (const node of nodes) {
      this.textFadeIn(node);
    }
  }

  textFadeIn(node: HTMLElement, tag: string = 'span', delay: number = 30) {

    if (typeof node.childNodes[0].nodeValue === 'string') {
      const text = node.childNodes[0].nodeValue;

      node.replaceChild(this.create(text, tag, delay), node.firstChild);
    }
  }

  create(text: string, tag: string, delay?: number) {

    const frag = document.createDocumentFragment();
    let delayCounter = 0;
    for (let j = 0, wordArr = text.trim().split(' '); j < wordArr.length; j++) {
      const wordWrapper = document.createElement('label');
      wordWrapper.setAttribute('class', 'word-wrapper');


      for (let i = 0, textArr = wordArr[j].split(''); i < textArr.length; i++) {
        const temp = document.createElement(tag);
        if (delay) {
          temp.setAttribute('style', 'transition-delay:' + (delay * (delayCounter + 1)) + 'ms');
          delayCounter++;
        }
        temp.innerHTML = textArr[i];
        wordWrapper.appendChild(temp);
      }

      const whiteSpace = document.createElement('span');
      whiteSpace.innerHTML = '&nbsp';

      wordWrapper.appendChild(whiteSpace);
      frag.appendChild(wordWrapper);
    }

    return frag;
  }

  sendEmail(formRef): Observable<IMessage> | any {
    const message: IMessage = {
      name: formRef.controls.name.value,
      email: formRef.controls.email.value,
      subject: formRef.controls.subject.value,
      message: formRef.controls.message.value,
    };

    return this.http.post(this.emailUrl, message)
      .map(response => {
        return response;
      })
      .catch(error => {
        return Observable.throw(error);
      });
  }

  getDeviceInfo() {
    return this.deviceService.getDeviceInfo();
  }

  // TODO poprawić zachowanie menu jak się wraca z widoku mobilnego
  // TODO poprawić googleMaps marker i pozycje
  // TODO add favicon
  // TODO poprawić animację przejścia z overview na page
  // TODO dodać stronę 404
  // TODO OPTIONAL animateText poprawić funkcje
}
