export interface CurrentPage {
  name: string;
  title: string;
  shortDescription: string;
  router: string;
  button: string;
}

export interface IMessage {
  name?: string;
  email?: string;
  subject?: string;
  message?: string;
}

export interface Skill {
  name: string;
  img: string;
}
