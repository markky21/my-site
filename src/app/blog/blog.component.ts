import {Component, OnInit, ViewChild} from '@angular/core';
import {AppService} from '../app.service';

@Component({
  selector: 'app-blog',
  template: `
    <section>
      <div #blogText class="blog-text">
        <img class="blog-mobile-image" src="../../assets/imgs/me.jpg" alt="">
        <h3 #blogPageDesc class="blogPageText fade-in-animation">The work is important, but it can not be put above everything :)</h3>
        <h3 #blogPageExtraText class="blogExtraText fade-in-animation">I hope you agree with me. If you want to see what interests me or what fascinates me, you can click on the button below..</h3>
        <a href="http://marcin-mirecki-blog.pl" target="_blank">
          <button #blogPageBtn class="btn"><span>take me to your blog site!</span></button>
        </a>
      </div>
      <div #blogPageImg class="blog-image">
        <img src="../../assets/imgs/me.jpg" alt="">
      </div>
    </section>
  `,
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {

  @ViewChild('blogPageDesc') blogPageDesc;
  @ViewChild('blogPageExtraText') blogPageExtraText;
  @ViewChild('blogPageBtn') blogPageBtn;

  constructor(private appService: AppService) {
  }

  ngOnInit() {
    this.appService.animateText([this.blogPageDesc.nativeElement, this.blogPageExtraText.nativeElement]);
    setTimeout(() => {
      $(this.blogPageDesc.nativeElement).addClass('animate');
      setTimeout(() => {
        $(this.blogPageExtraText.nativeElement).addClass('animate');
        setTimeout(() => {
          $(this.blogPageBtn.nativeElement).addClass('show');
        }, 3000);
      }, 2000);
    }, 2000);
  }

}
