import {Component, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-logo-main',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss']
})
export class LogoComponent implements OnInit {

  @ViewChild('svgLongSin') svgLongSin: any;
  path;
  length: number;
  addLetterClass = false;
  moveSin = false;

  constructor() {
  }

  ngOnInit() {
    setTimeout(() => {
      this.animateStraight();
      this.addLetterClass = true;
    }, 0);
  }

  animateStraight() {
    this.path = this.svgLongSin.nativeElement;
    this.length = this.path.getTotalLength();

    this.path.style.transition = this.path.style.WebkitTransition =
      'none';

    this.path.style.strokeDasharray = this.length + ' ' + this.length;
    this.path.style.strokeDashoffset = this.length;

    this.path.getBoundingClientRect();
    this.path.style.transition = this.path.style.WebkitTransition =
      'stroke-dashoffset 1s ease-in-out';
    this.path.style.strokeDashoffset = '0';
  }

//  TODO zamienić animacje napisu funkcją jsową

}
