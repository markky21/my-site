import {animate, query, style, transition, trigger, keyframes} from '@angular/animations';

export const initAnimation = trigger('initAnimation', [

  transition('show => hide', [
    query('.container', [
      style({
        left: 0,
      }),
      animate('300ms ease', style({
        left: '-100%',
      }))
    ]),
  ]),

  transition('hide => show', [
    query('.container', [
      style({
        left: '100%',
      }),
      animate('300ms ease', style({
        left: 0
      }))
    ]),
  ])

  // transition('hide => show', [
  //   query('.container', [
  //     style({
  //       left: '100%',
  //     }),
  //     animate('1200ms ease', keyframes([
  //       style({left: '100%', offset: 0}),
  //       style({left: '0', offset: 0.20}),
  //       style({left: '0', offset: 0.95}),
  //       style({left: '-100%', offset: 1}),
  //     ]))
  //   ])
  // ])
]);
