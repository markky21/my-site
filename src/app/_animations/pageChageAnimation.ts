import {animate, style, transition, trigger, query} from '@angular/animations';

export const pageChangeAnimation = trigger('pageChangeAnimation', [
  transition('* <=> *', [

    query(':leave',
      animate('1000ms ease',
        style({})
      ),
      {optional: true}),
  ])
]);
