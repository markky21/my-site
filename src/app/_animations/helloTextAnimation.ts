import {animate, group, keyframes, style, transition, trigger, state, query} from '@angular/animations';

export const helloTextAnimation = trigger('helloTextAnimation', [

  state('visible', style({
    opacity: 1,
    visibility: 'visible',
  })),
  state('hide', style({
    opacity: 0,
    visibility: 'hidden',
  })),
  transition('visible => hide', [
    animate(1000, style({
      opacity: 0,
      visibility: 'hidden',
      display: 'none'
    }))
  ])
]);
