import {animate, style, transition, trigger, state} from '@angular/animations';

export const mainNavAnimation = trigger('mainNavAnimation', [

  state('hidden', style({
    opacity: 0,
    visibility: 'hidden',
  })),
  state('visible', style({
    opacity: 1,
    visibility: 'visible',
    display: 'block',
  })),
  transition(':leave', [
    animate(1000, style({
      opacity: 0,
      visibility: 'hidden',
      display: 'none'
    }))
  ]),
  transition('hidden => visible', [
    animate(1000, style({
      opacity: 1,
      visibility: 'visible',
    }))
  ])
]);
