import {animate, style, transition, trigger} from '@angular/animations';

export const initAnimation = trigger('initAnimation', [

  transition(':leave', [
    animate(1000, style({
      opacity: 0,
      visibility: 'hidden',
      display: 'none'
    }))
  ])
]);
