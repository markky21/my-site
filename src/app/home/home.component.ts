import {Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import {pageChangeAnimation} from '../_animations/pageChageAnimation';
import {AppService} from '../app.service';
import {Subscription} from 'rxjs/Subscription';
import * as $ from 'jquery';

import {CurrentPage} from '../_interfaces/interfaces';


@Component({
  selector: 'app-home',
  template: `
    <article
      #homeArticle
      [@pageChangeAnimation]="getRouteAnimation(route)"
      (@pageChangeAnimation.start)="animationStarted($event)"
      class="home-content">
      <router-outlet #route="outlet"></router-outlet>
    </article>
  `,
  styleUrls: ['./home.component.scss'],
  animations: [
    pageChangeAnimation,
  ]
})
export class HomeComponent implements OnInit, OnDestroy {

  @ViewChild('homeArticle') homeArticle;
  currentPageSubscription: Subscription;
  currentPage: CurrentPage;

  constructor(private appService: AppService) {
  }

  ngOnDestroy() {
    this.currentPageSubscription.unsubscribe();
  }

  ngOnInit() {
    this.currentPageSubscription = this.appService.getCurrentPage()
      .subscribe((currentPage) => {
        if (!this.currentPage || this.currentPage.router !== currentPage.router) {
          this.currentPage = currentPage;
          $('html,body').animate(
            {scrollTop: 0},
            'slow',
            'swing',
            () => {
            });
        }
      });
  }

  animationStarted($event) {
    const target = $('app-' + $event.toState);

    $(this.homeArticle.nativeElement)
      .css('transition', 'opacity ' + $event.totalTime + 'ms ease')
      .css('opacity', 0);

    setTimeout(() => {
      target.css('display', 'block');
      setTimeout(() => {
        $(this.homeArticle.nativeElement).css('opacity', 1);
      }, 400);
    }, $event.totalTime + 100);
  }

  getRouteAnimation(outlet) {
    if (outlet.activatedRouteData) {
      return outlet.activatedRouteData.animation;
    }
  }

}
