import {Component, OnInit, ViewChild} from '@angular/core';
import {AppService} from '../app.service';
import {helloTextAnimation} from '../_animations/helloTextAnimation';

@Component({
  selector: 'app-home-text',
  template: `
    <section>
      <div #homeHelloText class="hello-text">
        <h2 #homePageTitle class="homePageTitle fade-in-animation">Hello there!</h2>
        <h3 #homePageDesc class="homePageText fade-in-animation">My name is Marcin Mirecki and I'm a web developer..</h3>
        <h3 #homePageExtraText class="homeExtraText fade-in-animation">... and yes, this is my cat. :)</h3>
        <button class="btn" #button routerLink="/home/overview"><span>let's go champ.</span></button>
      </div>
      <div #homePageImg class="hello-image">
        <img src="../../assets/imgs/Janke.jpg" alt="">
      </div>
    </section>
  `,
  styleUrls: [
    'home-text.component.scss'
  ],
  animations: [
    helloTextAnimation,
  ]
})
export class HomeTextComponent implements OnInit {


  @ViewChild('homePageTitle') homePageTitle;
  @ViewChild('homePageDesc') homePageDesc;
  @ViewChild('homeHelloText') homeHelloText;
  @ViewChild('homePageImg') homePageImg;
  @ViewChild('homePageExtraText') homePageExtraText;
  @ViewChild('button') button;

  constructor(private appService: AppService) {
  }

  ngOnInit() {

    this.appService.animateText([this.homePageTitle.nativeElement, this.homePageDesc.nativeElement, this.homePageExtraText.nativeElement]);
    setTimeout(() => {
      $(this.homePageTitle.nativeElement).addClass('animate');
      $(this.homePageImg.nativeElement).addClass('animate');
      setTimeout(() => {
        $(this.homePageDesc.nativeElement).addClass('animate');
        setTimeout(() => {
          $(this.homePageExtraText.nativeElement).addClass('animate');
          setTimeout(() => {
            $(this.button.nativeElement).addClass('show');
          }, 2000);
        }, 2500);
      }, 1000);
    }, 3000);
  }

}
