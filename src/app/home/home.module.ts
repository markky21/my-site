import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home.component';
import {SlickModule} from 'ngx-slick';
import {routerModule} from '../app.routing';
import {HomeTextComponent} from './home-text.component';
import {HomeNavComponent} from './home-nav.component';


@NgModule({
  imports: [
    CommonModule,
    SlickModule.forRoot(),
    routerModule
  ],
  declarations: [
    HomeComponent,
    HomeTextComponent,
    HomeNavComponent
  ],
  exports: [
    HomeComponent
  ]
})
export class HomeModule {
}
