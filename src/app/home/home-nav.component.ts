import {Component, OnInit, ViewChild, Renderer2} from '@angular/core';
import {AppService} from '../app.service';
import * as $ from 'jquery';

import {mainNavAnimation} from '../_animations/mainNavAnimation';
import {CurrentPage} from '../_interfaces/interfaces';

@Component({
  selector: 'app-home-nav',
  template: `
    <div #wrapper class="wrapper">
      <div class="nav-slide" [@mainNavAnimation]="navSlideState">

        <ngx-slick class="carousel" #slickModal="slick-modal" [config]="slideConfig"
                   (afterChange)="afterChange($event)">
          <div ngxSlickItem *ngFor="let site of sites" class="slide">
            <h2 (click)="selectSlide($event, site)">
              <span>{{site.name}}</span>
            </h2>
          </div>
        </ngx-slick>

      </div>

      <article #siteInfo class="site-info">
        <div *ngIf="currentSlide" class="text"><h3>{{currentSlide.title}}</h3>
          <p *ngIf="currentSlide.shortDescription !== ''">
            {{currentSlide.shortDescription}}
          </p>
        </div>
        <div *ngIf="currentSlide" class="button-wrapper">
          <button class="btn" routerLink="/page/{{currentSlide.router}}"><span>{{currentSlide.button}}</span></button>
        </div>
      </article>

    </div>
  `,
  styleUrls: [
    'home-nav.component.scss'
  ],
  animations: [
    mainNavAnimation,
  ]
})
export class HomeNavComponent implements OnInit {

  @ViewChild('slickModal') slickModal;
  @ViewChild('wrapper') wrapper;
  @ViewChild('siteInfo') siteInfo;

  navSlideState = 'hidden';
  $slick: JQuery;
  currentSlide: CurrentPage;
  sites: CurrentPage[];

  slideConfig = {
    'slidesToShow': 1,
    'slidesToScroll': 1,
    'variableWidth': true,
    'infinite': false,
    'dots': true,
  };

  constructor(public renderer: Renderer2, private appService: AppService) {
  }

  ngOnInit() {

    this.appService.getSites().subscribe(res => {
      this.sites = res.json().sites;
      if (!this.currentSlide) {
        this.currentSlide = this.sites[0];
      }
    });

    this.$slick = $(this.slickModal.el.nativeElement);
    this.renderer.listen('document', 'mousemove', (event) => {
      this.handleMouseMove(event);
    });

    setTimeout(() => {
      this.navSlideState = 'visible';
      this.contentToCenter();
    }, 500);
  }

  handleMouseMove(event): void  {
    const X = (event.clientX / window.innerWidth) * 20 + 40;
    const Y = (event.clientY / window.innerHeight) * 20 + 40;
    this.$slick.find('.slick-current span').css('background-position', `${X}% ${Y}%`);
  }

  selectSlide($event, site): void {
    $('.slick-dots').find('li:eq(' + $($event.path[2]).attr('data-slick-index') + ')').find('button').click();
    if (!this.currentSlide || this.currentSlide.name !== site.name) {
      const $siteInfo = $(this.siteInfo.nativeElement);
      $siteInfo.fadeOut(300, () => {
        this.currentSlide = site;
        $siteInfo.fadeIn(300);
      });
    }
  }

  contentToCenter(): void {
    const wrapper = $(this.wrapper.nativeElement);
    const wrapperH = wrapper.outerHeight();

    const headerH = $('app-main-nav header').outerHeight();
    const footerH = $('app-footer footer').outerHeight();
    const windowH = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

    if (windowH - footerH - headerH > wrapperH) {
      const marginTop = (windowH - footerH - headerH - wrapperH) / 4;
      wrapper.css('margin-top', marginTop);
    }
  }

  afterChange(event): void {
    $('.slick-current h2').click();
  }

}
