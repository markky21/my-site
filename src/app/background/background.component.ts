import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-background',
  template: `
    <div class="container">
      <div class="row">
        <div class="col-xs-12"></div>
      </div>
    </div>
  `,
  styleUrls: ['./background.component.scss']
})
export class BackgroundComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
