import {Component, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styles: [`

    .svg-long-sin-container {
      position: relative;
      height: 110px;
      width: 402px;
    }

    .svg-long-sin-wrapper {
      height: 100%;
      width: 100%;
      position: absolute;
      top: 0px;
      left: 0px;
    }

    .transform {
      transform: rotateX(-180deg);
    }

    svg {
      overflow: visible;
      height: 100%;
    }
  `]
})
export class LogoComponent implements OnInit {

  @ViewChild('svgLongSin') svgLongSin: any;
  path;
  length: number;

  moveSin = false;

  constructor() {
  }

  ngOnInit() {
    setTimeout(() => {
      this.animateStraight();
      setTimeout(() => {
      }, 1000);
    }, 0);
  }

  animateStraight() {
    this.path = this.svgLongSin.nativeElement;
    this.length = this.path.getTotalLength();

    this.path.style.transition = this.path.style.WebkitTransition =
      'none';

    this.path.style.strokeDasharray = this.length + ' ' + this.length;
    this.path.style.strokeDashoffset = this.length;

    this.path.getBoundingClientRect();
    this.path.style.transition = this.path.style.WebkitTransition =
      'stroke-dashoffset 1s ease-in-out';
    this.path.style.strokeDashoffset = '0';
  }

  toggleClass() {
    this.moveSin = !this.moveSin;
  }
}
