import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaveBockgroundComponent } from './wave-bockground.component';

describe('WaveBockgroundComponent', () => {
  let component: WaveBockgroundComponent;
  let fixture: ComponentFixture<WaveBockgroundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaveBockgroundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaveBockgroundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
