import {Component, OnInit, ViewChild, HostListener} from '@angular/core';

@Component({
  selector: 'app-wave-bockground',
  template: `
    <div #sinWrapper class="sin-wrapper">
      <svg #svg width="100%" height="120">
        <!--<path #path/>-->
        <polygon #path/>
      </svg>
    </div>
  `,
  styles: [`
    svg {
      transform: rotate(-180deg) translateY(10px);
      display: inline-block;
      stroke-width: 1px;
      fill: rgba(255,255,255,0.5);
      stroke: #ececec;
      overflow: visible;
    }

    div.sin-wrapper {
      align-items: center;
      justify-content: center;
      overflow: hidden;
      margin: 0;
    }
  `],
})
export class WaveBockgroundComponent implements OnInit {

  @ViewChild('svg') svg;
  @ViewChild('sinWrapper') sinWrapper;
  @ViewChild('path') path;

  counter: number;
  width: number;
  height: number;
  positionInfo;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.getSize();
  }

  constructor() {
  }

  ngOnInit() {
    this.svg = this.svg.nativeElement
    this.path = this.path.nativeElement
    this.getSize();
    this.counter = Math.floor(Math.random() * 1000000000);

    setInterval(() => {
      this.draw();
      this.counter = this.counter++;
    }, 50);

  }

  getSize() {
    this.positionInfo = this.svg.getBoundingClientRect();
    this.width = this.positionInfo.width;
    this.height = this.positionInfo.height;
  }


  move(amplitude: number, frequency: number, length: number, i) {
    const point: Array<any> = [];
    const width = length;
    let x = 0, y;
    while (x++ <= width) {
      y = Math.sin(x * frequency + i);
      point.push([(x), (y * amplitude / 10 + amplitude / 2)].join(' '));
    }
    point.push([length, amplitude * 4].join(' '));
    point.push([0, amplitude * 4].join(' '));
    return point;
  }

  draw() {
    const c = ++this.counter / 30;
    this.path.setAttribute('points', this.move(this.height / 1.5, this.width / 1000000, this.width, c));
    // this.path.setAttribute('d', 'M ' + this.move(this.height, this.width / 1000000, this.width, c));
  }

}
