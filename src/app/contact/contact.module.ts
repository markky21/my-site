import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ContactComponent} from './contact.component';
import {AgmCoreModule} from '@agm/core';
import {FormsModule} from '@angular/forms';
import {MapService} from './map.service';

@NgModule({
  imports: [
    CommonModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC9TW11GwhG3wgkezzxoHjn3BTbl8IxhzA'
    }),
    FormsModule
  ],
  declarations: [ContactComponent],
  exports: [
    ContactComponent
  ],
  providers: [MapService]
})
export class ContactModule {
}
