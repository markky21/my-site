import {Component, OnInit} from '@angular/core';
import {AppService} from '../app.service';
import {MessageService} from '../message/message.service';
import {slideUpFadeIn} from '../_animations/slideUpFadeIn';
import {MapService} from "./map.service";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  animations: [slideUpFadeIn]
})
export class ContactComponent implements OnInit {

  offsetX = 51.107877;
  offsetY = 17.011900;
  lat: number = 51.101918;
  lng: number = 17.041140;
  mapSize: number = 14;
  mapStyles;

  animateText = false;

  constructor(private appService: AppService,
              private mapService: MapService,
              private messageService: MessageService) {
  }

  ngOnInit() {
    if (window.innerWidth < 768) {
      this.offsetX = 51.111918;
      this.offsetY = 17.041140;
    }

    setTimeout(() => {
      this.animateText = true;
    }, 2000);

    this.mapStyles = this.mapService.getMatStyles();
  }

  sentEmail(formRef) {
    console.log(formRef);
    if (!formRef.valid) {
      return;
    }
    this.appService.sendEmail(formRef)
      .subscribe(res => {
          // console.log('AppComponent Success', res);
          this.messageService.setCurrentMessage({
            message: 'Your message has been delivered! Thanks for the message :)',
            isError: false
          });
        }, error => {
          // console.log('AppComponent Error', error);
          this.messageService.setCurrentMessage({
            message: 'Upss .. something went wrong. Try to form the form once again.',
            isError: true
          });
        }
      );
  }

}
